<?php

namespace App\Service;
use App\Entity\Shape;

class GeometryCalculator
{

    function __construct() 
    {

    }

    public function sumCircumferences(Shape $shape1, Shape $shape2){
        return $shape1->getCircumference() + $shape2->getCircumference();
    }

    public function sumSurfaces(Shape $shape1, Shape $shape2){
        return $shape1->getSurface() + $shape2->getSurface();
    }
}


?>