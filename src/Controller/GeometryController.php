<?php

namespace App\Controller;

use App\Entity\Triangle;
use App\Entity\Circle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Service\GeometryCalculator;

/**
 * @Route("/")
 */
class GeometryController extends AbstractController
{
    /**
     * @Route("triangle/{a}/{b}/{c}", methods={"GET"})
     */
    public function triangle($a = 0, $b = 0, $c = 0, GeometryCalculator $gc): Response
    {
        try{
            $result = new Triangle($a, $b, $c);
        }catch(\Exception $e) {
            $result = json_encode(array('error' => $e->getMessage()));
        }

        $response = new Response($result);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("circle/{radius}", methods={"GET"})
     */
    public function circle($radius = 0, GeometryCalculator $gc): Response
    {
        try{
            $result = new Circle($radius);
        }catch(\Exception $e) {
            $result = json_encode(array('error' => $e->getMessage()));
        }



        $response = new Response($result);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
