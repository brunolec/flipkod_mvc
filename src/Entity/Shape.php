<?php

namespace App\Entity;



abstract class Shape
{

    abstract protected function getCircumference();
    abstract protected function getSurface();

    abstract protected function recalculation();
}
