<?php

namespace App\Entity;

use App\Repository\CircleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CircleRepository::class)
 */
class Circle extends Shape
{

    private $radius;

    private $circumference;

    private $surface;

    function __construct($radius) 
    {
        if(is_numeric($radius)){
            $this->radius = $radius;

            $this->recalculation();  // Calculate circumference and surface when object is created

        }else{
            throw new Exception('Argument are not numerical');
        }
    }

    public function getRadius(): float
    {
        return $this->radius;
    }

    public function setRadius(float $radius): self
    {
        $this->radius = $radius;

        $this->recalculation();  // Calculate circumference and surface when radius is changed

        return $this;
    }

    public function getCircumference(): float
    {
        return $this->circumference;
    }

    public function getSurface(): float
    {
        return $this->surface;
    }

    private function calculateCircumference()
    {
        $this->circumference = 2 * $this->radius + pi();
    }

    private function calculateSurface()
    {
        $this->surface = pi() * pow($this->radius, 2);
    }

    protected function recalculation()
    {
        $this->calculateCircumference();
        $this->calculateSurface();
    }

    public function __toString(){
        $arr = array(   "type"          => "circle",
                        "radius"        => number_format($this->radius, 2, '.', ' '),
                        "surface"       => number_format($this->circumference, 2, '.', ' '),
                        "circumference" => number_format($this->surface, 2, '.', ' '));
        return json_encode($arr);
    }
}
