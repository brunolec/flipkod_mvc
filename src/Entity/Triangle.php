<?php

namespace App\Entity;

use App\Repository\TriangleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpKernel\HttpKernel;
use Exception;



class Triangle extends Shape
{

    private $a;

    private $b;

    private $c;

    private $circumference;

    private $surface;


    function __construct($a, $b, $c) 
    {
        if(is_numeric($a) && is_numeric($b) && is_numeric($c)){
            if( $a + $b > $c && 
                $b + $c > $a && 
                $a + $c > $b){  //  The SUM of two sides must add up to be greather than the length of the remaining third side
                    
                $this->a = $a;
                $this->b = $b;
                $this->c = $c;


                $this->calculateCircumference();
                $this->calculateSurface();  // Calculate circumference and surface when object is created

            }else{
                throw new Exception('Not triangle');
            }
        }else{
            throw new Exception('Arguments are not numerical');
        }
    }

    public function getA(): float
    {
        return $this->a;
    }

    public function setA(float $a): self
    {
        $this->a = $a;

        $this->recalculation();  // Calculate circumference and surface when a is changed

        return $this;
    }

    public function getB(): float
    {
        return $this->b;
    }

    public function setB(float $b): self
    {
        $this->b = $b;

        $this->recalculation();  // Calculate circumference and surface when b is changed

        return $this;
    }

    public function getC(): float
    {
        return $this->c;
    }

    public function setC(float $c): self
    {
        $this->c = $c;

        $this->recalculation();  // Calculate circumference and surface when c is changed

        return $this;
    }

    public function getCircumference(): float
    {
        return $this->circumference;
    }

    public function getSurface(): float
    {
        return $this->surface;
    }

    private function calculateCircumference()
    {
        $this->circumference = $this->a + $this->b + $this->c;
    }

    private function calculateSurface()
    {

        $s = $this->getCircumference() / 2;   // Calculate semi-perimeter of Heron's formula

        $this->surface = sqrt($s * ($s - $this->a) * ($s - $this->b) * ($s - $this->c));
    }

    protected function recalculation()
    {
        $this->calculateCircumference();    //  calculateCircumference MUST be before calculateSurface because for calculation surface needs circumference
        $this->calculateSurface();
    }


    public function __toString(){
        $obj = array(   "type"          => "triangle",
                        "a"             => number_format($this->a, 2, '.', ' '),
                        "b"             => number_format($this->b, 2, '.', ' '),
                        "c"             => number_format($this->c, 2, '.', ' '),
                        "surface"       => number_format($this->surface, 2, '.', ' '),
                        "circumference" => number_format($this->circumference, 2, '.', ' '));
        return json_encode($obj);
    }
}
